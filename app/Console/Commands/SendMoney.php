<?php

namespace App\Console\Commands;

use App\Http\Controllers\TransferController;
use Illuminate\Console\Command;

class SendMoney extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = '900:send
                            {sender name : Имя отправителя} 
                            {receiver name : Имя получателя} 
                            {transfer sum : Сумма перевода}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Transfer money from the balance of one user to the balance of another. Powered by SBERBANK';

    protected $controller;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(TransferController $transferController)
    {
        parent::__construct();

        $this->controller = $transferController;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ( !is_numeric( $this->argument('transfer sum') )) { echo 'Error: Transfer sum must be numeric type'; exit(); }

        $this->controller->send($this->arguments());
    }
}

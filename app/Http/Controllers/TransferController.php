<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\User;

class TransferController extends Controller
{

    public function send(array $options = []){

        /**
         * $options = []
         * ['command='] => консольная команда
         * ['sender name'] => имя отправителя
         * ['receiver name'] => имя получателя
         * ['transfer sum'] => сумма перевода
         */

        $user1_balance = DB::table('Users')->where('name', $options['sender name'])->value('balance');
        $user2_balance = DB::table('Users')->where('name', $options['receiver name'])->value('balance');

        //is sender exist
        if ( User::where('name', $options['sender name'])->count() == 0 ) { echo 'Error: There is no such sender'; exit(); }
        //is recipient exist
        if ( User::where('name', $options['receiver name'])->count() == 0 ) { echo 'Error: There is no such recipient'; exit(); }
        //is money available
        if ( $user1_balance < $options['transfer sum']) { echo 'Error: Insufficient funds'; exit(); }

        DB::table('Users')->where('name', $options['sender name'])->update([ 'balance' => $user1_balance - $options['transfer sum'] ]);
        DB::table('Users')->where('name', $options['receiver name'])->update([ 'balance' => $user2_balance + $options['transfer sum'] ]);

        echo 'Success: Money transferred';
    }
}
